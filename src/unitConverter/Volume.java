package unitConverter;

public enum Volume implements Unit{
	CUBICMETER( "CubicMeter" , 1),
	CUBICKILOMERTER( "CubicKilometer" , 1000000000 ),
	CUBICCENTIMETER( "CubicCentimeter" , 0.000001 ),
	CUBICINCHES( "CubicInches" , 61023.744095 ),
	CUBICFEET( "CubicFeet" , 35.314667 ),
	THANG( "Thang" , 50 );
	
	private final String name;
	private final double value;

	Volume( String name , double value )
	{
		this.name = name;
		this.value = value;
	}
	
	@Override
	public double convertTo(Unit other, double amt) {
		return this.value*amt/other.getValue();
	}

	@Override
	public double getValue() {
		return this.value;
	}

}
