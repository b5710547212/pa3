package unitConverter;

public enum Area implements Unit{
	SQUAREMETER( "CubicMeter" , 1.00 ),//si unit
	SQUAREKILOMETER( "CubicKilometer" , 1000000.00 ),//si unit
	SQUARECENTIMETER( "SquareCentimeter" , 0.0001 ),//si unit
	SQUAREINCH( "SquareInch" , 1550.0031 ),//English unit
	SQUAREFOOT( "SquareFoot" , 10.76391 ),//Engish unit
	NGAN( "Ngan" , 0.25 );//thai unit

	private final String name;
	private final double value;
	
	Area( String name , double value )
	{
		this.name = name;
		this.value = value;
	}
	
	@Override
	public double convertTo(Unit other, double amt) {
		return this.value*amt/other.getValue();
	}

	@Override
	public double getValue() {
		return this.value;
	}

}
