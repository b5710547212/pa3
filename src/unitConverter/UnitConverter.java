package unitConverter;
/**
 * 
 * @author Woramate Jumroonsilp
 *
 */
public class UnitConverter {

	/**
	 * 
	 * @param amt
	 * @param fromUnit
	 * @param toUnit
	 * @return
	 */
	public double convert( double amt , Unit fromUnit , Unit toUnit)
	{
		return fromUnit.convertTo(toUnit, amt);
	}
	
	/**
	 * 
	 * @param other
	 * @return
	 */
	public Unit[] getUnit( UnitType other )
	{
		return other.unit;
	}
}
