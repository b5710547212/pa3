package unitConverter;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ConverterUI extends JFrame{

	private UnitConverter unitConverter;
	
	private JPanel mainPanel;
	private JTextField leftField, rightField;
	private JLabel equalLabel;
	private JButton convBtn, clrBtn;
	private JComboBox leftBox, rightBox;
	private JMenuBar bar;
	private JMenu unitMenu;
	private JMenuItem lenghtMenu, areaMenu, weightMenu, volumeMenu, exitMenu;
	private int Side;
	private String leftStr = "", rightStr = "";
	
	public ConverterUI( UnitConverter uc )
	{
		
		this.unitConverter = uc;
		
		initComponent();
		this.setTitle("Unit Converter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		this.pack();
		
	}
	
	public void initComponent()
	{
		
		this.mainPanel = new JPanel();
		this.mainPanel.setLayout( new FlowLayout() );
		
		this.leftField = new JTextField(10);
		this.rightField = new JTextField(10);
		
		this.equalLabel = new JLabel("=");
		
		this.convBtn = new JButton("Convert!");
		this.clrBtn = new JButton("Clear");
		
		this.leftBox = new JComboBox();
		this.rightBox = new JComboBox();
		
		this.bar = new JMenuBar();
		
		this.unitMenu = new JMenu("Unit Type");
		this.lenghtMenu = new JMenuItem("Lenght");
		this.areaMenu = new JMenuItem("Area");
		this.weightMenu = new JMenuItem("Weight");
		this.volumeMenu = new JMenuItem("Volume");
		this.exitMenu = new JMenuItem("Exit");
		
		convBtn.addActionListener( new ConvertButtonListener() );
		clrBtn.addActionListener( new ClrButtonListener() );
		leftField.addActionListener( new ConvertButtonListener() );
		rightField.addActionListener( new ConvertButtonListener() );
		
		this.unitMenu.add(lenghtMenu);
		this.unitMenu.add(areaMenu);
		this.unitMenu.add(weightMenu);
		this.unitMenu.add(volumeMenu);
		this.unitMenu.addSeparator();
		this.unitMenu.add(this.exitMenu);
		
		this.lenghtMenu.addActionListener( new MenuListener( UnitType.LenghtUnit ) );
		this.areaMenu.addActionListener( new MenuListener( UnitType.AreaUnit ) );
		this.weightMenu.addActionListener( new MenuListener( UnitType.WeightUnit ) );
		this.volumeMenu.addActionListener( new MenuListener( UnitType.VolumeUnit ) );
		this.exitMenu.addActionListener( new exitMenuListener() );
		
		this.bar.add(unitMenu);
		
		this.mainPanel.add(this.leftField);
		this.mainPanel.add(this.leftBox);
		this.mainPanel.add(this.equalLabel);
		this.mainPanel.add(this.rightField);
		this.mainPanel.add(this.rightBox);
		this.mainPanel.add(this.convBtn);
		this.mainPanel.add(this.clrBtn);
		
		super.setJMenuBar(bar);
		super.add(mainPanel);
		
	}
	
	public void run()
	{
		super.setVisible(true);
	}
	
	public static void main(String[] args)
	{
		ConverterUI cu = new ConverterUI(new UnitConverter());
		cu.run();
	}
	
	class ConvertButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			try
			{
				if((leftStr.equals("")&&!rightStr.equals(""))||leftStr.equals(leftField.getText()))
				{
					double input = Double.parseDouble(rightField.getText());
					if(input > 0)
					{
						leftField.setText(unitConverter.convert(input,((Unit)rightBox.getSelectedItem()),((Unit)leftBox.getSelectedItem()))+"");
					}
					else{
						JOptionPane.showMessageDialog(null,"Number not more than 0","Error",JOptionPane.CLOSED_OPTION);
					}
				}
				else if((rightStr.equals("")&&!leftField.equals(""))||rightStr.equals(rightField.getText()))
				{
					double input = Double.parseDouble(leftField.getText());
					if(input > 0)
					{
						rightField.setText(unitConverter.convert(input,((Unit)leftBox.getSelectedItem()),((Unit)rightBox.getSelectedItem()))+"");
					}
					else{
						JOptionPane.showMessageDialog(null,"Number not more than 0","Error",JOptionPane.CLOSED_OPTION);
					}
				}
			}catch(NumberFormatException e1)
			{
				JOptionPane.showMessageDialog(null,"NaN","Error",JOptionPane.CLOSED_OPTION);
			}
			leftStr = leftField.getText();
			rightStr = rightField.getText();
		}
	}
	
	class ClrButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			leftField.setText(null);
			rightField.setText(null);
		}
	}
	
	class MenuListener implements ActionListener{
		public Unit[] temp;
		public MenuListener( UnitType other ){
			this.temp = other.unit;
		}
		public void actionPerformed(ActionEvent e){
			for( Unit a : temp )
			{
				leftBox.addItem(a);
				rightBox.addItem(a);
			}
			pack();
		}
	}
	
	class exitMenuListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	}
}
