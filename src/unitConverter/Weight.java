package unitConverter;

public enum Weight implements Unit{
	GRAMS( "Gram" , 0.001 ),//si unit
	KILOGRAMS( "KiloGram" , 1),//si unit
	TONS( "Ton" , 0.000984 ),//si unit
	OUNCES( "Ounces" , 35.273962 ),//English unit
	POUNDS( "Pounds" , 2.204623 ),//English unit
	CHANG( "Chang" , 0.83333 )//Thai unit
	;
	
	private final String name;
	private final double value;
	
	Weight( String name , double value )
	{
	this.name = name;
	this.value = value;
	}
	
	@Override
	public double convertTo(Unit other, double amt) {
		return this.value*amt/other.getValue();
	}

	@Override
	public double getValue() {
		return this.value;
	}

}
