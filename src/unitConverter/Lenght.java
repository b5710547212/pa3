package unitConverter;
/**
 * Enum of lenght include meter, kilometer, centimeter, foot, mile and wa.
 * @author Woramate Jumroonsilp
 *
 */
public enum Lenght implements Unit{
	/*
	 *
	 */
	METER("meter", 1.0),//is unit
	KILOMETER("kilometer", 1000.0),//si unit
	CENTIMETER("centimeter", 0.01),//si unit
	MILE("mile", 1609.344),//english unit
	FOOT("foot", 0.30480),//english unit
	WA("wa", 2.0);//thai unit

	/**
	 * Valiable that contain information of this unit.
	 */
	private final String name;
	private final double value;
	
	/**
	 * Constructor for create this unit
	 * @param name
	 * @param value
	 */
	Lenght( String name , double value)
	{
		this.name = name;
		this.value = value;
	}
	
	/**
	 * Use to comnvert form this unit to other unit.
	 */
	@Override
	public double convertTo(Unit other, double amt) {
		return this.value*amt/other.getValue();
	}

	
	/**
	 * reuturn value of this unit.
	 */
	@Override
	public double getValue() {
		return this.value;
	}

}
