package unitConverter;

public enum UnitType {
	
	LenghtUnit( Lenght.values() ),
	AreaUnit( Area.values() ),
	WeightUnit( Weight.values() ),
	VolumeUnit( Volume.values() )
	;
	
	public final Unit[] unit;
	
	private UnitType( Unit[] other )
	{
		this.unit = other;
	}
	
}
